#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/wait.h>
#include <sys/types.h>

#define assertsyscall(x, y) if((x)y){int err = errno; {perror(#x); exit(err);}}

/* child.c */

int main()
{
    assertsyscall(kill(getppid(), SIGINT), <0);
    assertsyscall(kill(getppid(), SIGQUIT), <0);
    assertsyscall(kill(getppid(), SIGILL), <0);
    assertsyscall(kill(getppid(), SIGUSR1), <0);
    assertsyscall(kill(getppid(), SIGUSR2), <0);
    assertsyscall(kill(getppid(), SIGABRT), <0);
    assertsyscall(kill(getppid(), SIGABRT), <0);
    assertsyscall(kill(getppid(), SIGABRT), <0);

    return 0;
}
