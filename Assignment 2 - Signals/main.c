#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/wait.h>
#include <sys/types.h>

#define assertsyscall(x, y) if((x)y){int err = errno; {perror(#x); exit(err);}}

/* main2.c is a program that handles signals sent to it*/

void signalHandler(int signum)
{
    assertsyscall(printf("Handling signal: %d\n", signum), <=0);

    switch(signum)
    {
	case SIGINT:
		assertsyscall(printf("	SIGINT: interrupt\n"), <=0);
		break;

	case SIGQUIT:
		assertsyscall(printf("	SIGQUIT: quit\n"), <=0);
		break;

	case SIGILL:
		assertsyscall(printf("	SIGILL: illegal instruction\n"), <=0);
		break;

	case SIGUSR1:
		assertsyscall(printf("	SIGUSR1: user defined signal 1\n"), <=0);
		break;

	case SIGUSR2:
		assertsyscall(printf("	SIGUSR2: user defined signal 2\n"), <=0);
		break;

	case SIGABRT:
		assertsyscall(printf("	SIGABRT: Abort trap\n"), <=0);
		break;

	default:
		assertsyscall(printf("Unhandled signal: %d\n", signum), <=0);
		break;
    }
}

int main()
{
    signal(SIGINT, signalHandler);
    signal(SIGQUIT, signalHandler);
    signal(SIGILL, signalHandler);
    signal(SIGUSR1, signalHandler);
    signal(SIGUSR2, signalHandler);
    signal(SIGABRT, signalHandler);
    
    pid_t cpid;

    cpid = fork();

    if(cpid == 0) /* child process */
    {
	assertsyscall(execl("./child","child", (char *) NULL), ==0);
    }

    else /* Parent process. Parent will wait for child to complete.*/
    {
		while (1)
		{
			int result = wait(NULL);
			if (result == -1)
			{
				if (errno == EINTR)
				{
					continue;
				}
			}
			else
			{
				break;
			}
		}
		assertsyscall(printf("Child process complete.\n"), <=0);
    }

    return 0;
}
