#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>
#define assertsyscall(x, y) if((x)y){int err = errno; {perror(#x); exit(err);}}

int main(int arg_count, char* arg_vector[])
{
    int n = 0;

    if(arg_count <= 1)
    {
	assertsyscall(printf("Please provide command line arguments\n"), <= 0);
	exit(-1);
    }

    else
    {
	for(n = 0; n < atoi(arg_vector[1]); n++)
	{
	    assertsyscall(printf("Process: %d %d\n", getpid(), n + 1), <= 0);
	}

	//assertsyscall(printf("Process %d exited with status: %d\n", getpid(), n), <= 0);
	exit(n);
    }

    return 0;
}
