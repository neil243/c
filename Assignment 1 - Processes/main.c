#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>
#define assertsyscall(x, y) if((x)y){int err = errno; {perror(#x); exit(err);}}

int main()
{
    pid_t child_pid;
    pid_t rc_pid;

    /* fork a child process */
    child_pid = fork();

    if(child_pid < 0) /*error occured*/
    {
	perror("fork() failed");
	exit(-1);
    }

    else if(child_pid == 0) /* child process */
    {
	assertsyscall(printf("Child PID: %d\n", getpid()), <= 0);
	assertsyscall(printf("Parent PID: %d\n", getppid()), <= 0);

	assertsyscall(execl("./counter","./counter", "5", (char *) NULL), > 0);
    }

    /* parent process. parent will wait for child to complete.*/
    else
    {
	/* check status of child process */
	int status;
	rc_pid = wait(&status);

	if(rc_pid > 0)
	{
	    if(WIFEXITED(status))
		assertsyscall(printf("Child Process %d exited with status: %d\n", 			child_pid, WEXITSTATUS(status)), <= 0);
	}

	/* if no PID returned, then an error occured */
	else
	{
	    if(errno == ECHILD)
	    {
		assertsyscall(printf("No children exist.\n"), <= 0);
	    }

	    else
	    {
		assertsyscall(printf("Unexpected error."), <= 0);
		abort();
	    }
	}


    }

    return 0;
}
